function cp
  rsync --progress --size-only --inplace --verbose $argv
end
