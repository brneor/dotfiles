function dark -d "Set dark theme"
  set -xU theme "dark"
  kitty @ set-colors -a "~/.config/kitty/themes/kanagawa_dark.conf"
end
