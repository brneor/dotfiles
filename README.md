Utilize o [stow](https://www.gnu.org/software/stow/) para criar os links para os diretórios corretos.

Exemplo:

```bash
stow lazydocker
```

Para testar antes de efetivar as mudanças, utilize o parâmetro `-n` ou `--no` (é bom utilizar junto com o `-v` ou `--verbose=N` para identificar potenciais falhas):

```bash
stow -nv lazydocker
```
